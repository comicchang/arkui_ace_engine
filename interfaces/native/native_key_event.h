/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup ArkUI_NativeModule
 * @{
 *
 * @brief 提供ArkUI在Native侧的通用按键事件能力。
 *
 * @since 14
 */

/**
 * @file native_key_event.h
 *
 * @brief 提供NativeKeyEvent相关接口定义。
 *
 * @library libace_ndk.z.so
 * @syscap SystemCapability.ArkUI.ArkUI.Full
 * @since 14
 */

#ifndef ARKUI_NATIVE_KEY_EVENT_H
#define ARKUI_NATIVE_KEY_EVENT_H

#include <stdint.h>

#include "native_type.h"
#include "ui_input_event.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
    ARKUI_KEYCODE_KEY_UNKNOWN = -1,
    ARKUI_KEYCODE_KEY_FN = 0,
    ARKUI_KEYCODE_KEY_HOME = 1,
    ARKUI_KEYCODE_KEY_BACK = 2,
    ARKUI_KEYCODE_KEY_CALL = 3,
    ARKUI_KEYCODE_KEY_ENDCALL = 4,
    ARKUI_KEYCODE_KEY_CLEAR = 5,
    ARKUI_KEYCODE_KEY_HEADSETHOOK = 6,
    ARKUI_KEYCODE_KEY_FOCUS = 7,
    ARKUI_KEYCODE_KEY_NOTIFICATION = 8,
    ARKUI_KEYCODE_KEY_SEARCH = 9,
    ARKUI_KEYCODE_KEY_MEDIA_PLAY_PAUSE = 10,
    ARKUI_KEYCODE_KEY_MEDIA_STOP = 11,
    ARKUI_KEYCODE_KEY_MEDIA_NEXT = 12,
    ARKUI_KEYCODE_KEY_MEDIA_PREVIOUS = 13,
    ARKUI_KEYCODE_KEY_MEDIA_REWIND = 14,
    ARKUI_KEYCODE_KEY_MEDIA_FAST_FORWARD = 15,
    ARKUI_KEYCODE_KEY_VOLUME_UP = 16,
    ARKUI_KEYCODE_KEY_VOLUME_DOWN = 17,
    ARKUI_KEYCODE_KEY_POWER = 18,
    ARKUI_KEYCODE_KEY_CAMERA = 19,
    ARKUI_KEYCODE_KEY_VOICE_ASSISTANT = 20,
    ARKUI_KEYCODE_KEY_CUSTOM1 = 21,
    ARKUI_KEYCODE_KEY_VOLUME_MUTE = 22,
    ARKUI_KEYCODE_KEY_MUTE = 23,
    ARKUI_KEYCODE_KEY_BRIGHTNESS_UP = 40,
    ARKUI_KEYCODE_KEY_BRIGHTNESS_DOWN = 41,
    ARKUI_KEYCODE_KEY_WEAR_1 = 1001,
    ARKUI_KEYCODE_KEY_0 = 2000,
    ARKUI_KEYCODE_KEY_1 = 2001,
    ARKUI_KEYCODE_KEY_2 = 2002,
    ARKUI_KEYCODE_KEY_3 = 2003,
    ARKUI_KEYCODE_KEY_4 = 2004,
    ARKUI_KEYCODE_KEY_5 = 2005,
    ARKUI_KEYCODE_KEY_6 = 2006,
    ARKUI_KEYCODE_KEY_7 = 2007,
    ARKUI_KEYCODE_KEY_8 = 2008,
    ARKUI_KEYCODE_KEY_9 = 2009,
    ARKUI_KEYCODE_KEY_STAR = 2010,
    ARKUI_KEYCODE_KEY_POUND = 2011,
    ARKUI_KEYCODE_KEY_DPAD_UP = 2012,
    ARKUI_KEYCODE_KEY_DPAD_DOWN = 2013,
    ARKUI_KEYCODE_KEY_DPAD_LEFT = 2014,
    ARKUI_KEYCODE_KEY_DPAD_RIGHT = 2015,
    ARKUI_KEYCODE_KEY_DPAD_CENTER = 2016,
    ARKUI_KEYCODE_KEY_A = 2017,
    ARKUI_KEYCODE_KEY_B = 2018,
    ARKUI_KEYCODE_KEY_C = 2019,
    ARKUI_KEYCODE_KEY_D = 2020,
    ARKUI_KEYCODE_KEY_E = 2021,
    ARKUI_KEYCODE_KEY_F = 2022,
    ARKUI_KEYCODE_KEY_G = 2023,
    ARKUI_KEYCODE_KEY_H = 2024,
    ARKUI_KEYCODE_KEY_I = 2025,
    ARKUI_KEYCODE_KEY_J = 2026,
    ARKUI_KEYCODE_KEY_K = 2027,
    ARKUI_KEYCODE_KEY_L = 2028,
    ARKUI_KEYCODE_KEY_M = 2029,
    ARKUI_KEYCODE_KEY_N = 2030,
    ARKUI_KEYCODE_KEY_O = 2031,
    ARKUI_KEYCODE_KEY_P = 2032,
    ARKUI_KEYCODE_KEY_Q = 2033,
    ARKUI_KEYCODE_KEY_R = 2034,
    ARKUI_KEYCODE_KEY_S = 2035,
    ARKUI_KEYCODE_KEY_T = 2036,
    ARKUI_KEYCODE_KEY_U = 2037,
    ARKUI_KEYCODE_KEY_V = 2038,
    ARKUI_KEYCODE_KEY_W = 2039,
    ARKUI_KEYCODE_KEY_X = 2040,
    ARKUI_KEYCODE_KEY_Y = 2041,
    ARKUI_KEYCODE_KEY_Z = 2042,
    ARKUI_KEYCODE_KEY_COMMA = 2043,
    ARKUI_KEYCODE_KEY_PERIOD = 2044,
    ARKUI_KEYCODE_KEY_ALT_LEFT = 2045,
    ARKUI_KEYCODE_KEY_ALT_RIGHT = 2046,
    ARKUI_KEYCODE_KEY_SHIFT_LEFT = 2047,
    ARKUI_KEYCODE_KEY_SHIFT_RIGHT = 2048,
    ARKUI_KEYCODE_KEY_TAB = 2049,
    ARKUI_KEYCODE_KEY_SPACE = 2050,
    ARKUI_KEYCODE_KEY_SYM = 2051,
    ARKUI_KEYCODE_KEY_EXPLORER = 2052,
    ARKUI_KEYCODE_KEY_ENVELOPE = 2053,
    ARKUI_KEYCODE_KEY_ENTER = 2054,
    ARKUI_KEYCODE_KEY_DEL = 2055,
    ARKUI_KEYCODE_KEY_GRAVE = 2056,
    ARKUI_KEYCODE_KEY_MINUS = 2057,
    ARKUI_KEYCODE_KEY_EQUALS = 2058,
    ARKUI_KEYCODE_KEY_LEFT_BRACKET = 2059,
    ARKUI_KEYCODE_KEY_RIGHT_BRACKET = 2060,
    ARKUI_KEYCODE_KEY_BACKSLASH = 2061,
    ARKUI_KEYCODE_KEY_SEMICOLON = 2062,
    ARKUI_KEYCODE_KEY_APOSTROPHE = 2063,
    ARKUI_KEYCODE_KEY_SLASH = 2064,
    ARKUI_KEYCODE_KEY_AT = 2065,
    ARKUI_KEYCODE_KEY_PLUS = 2066,
    ARKUI_KEYCODE_KEY_MENU = 2067,
    ARKUI_KEYCODE_KEY_PAGE_UP = 2068,
    ARKUI_KEYCODE_KEY_PAGE_DOWN = 2069,
    ARKUI_KEYCODE_KEY_ESCAPE = 2070,
    ARKUI_KEYCODE_KEY_FORWARD_DEL = 2071,
    ARKUI_KEYCODE_KEY_CTRL_LEFT = 2072,
    ARKUI_KEYCODE_KEY_CTRL_RIGHT = 2073,
    ARKUI_KEYCODE_KEY_CAPS_LOCK = 2074,
    ARKUI_KEYCODE_KEY_SCROLL_LOCK = 2075,
    ARKUI_KEYCODE_KEY_META_LEFT = 2076,
    ARKUI_KEYCODE_KEY_META_RIGHT = 2077,
    ARKUI_KEYCODE_KEY_FUNCTION = 2078,
    ARKUI_KEYCODE_KEY_SYSRQ = 2079,
    ARKUI_KEYCODE_KEY_BREAK = 2080,
    ARKUI_KEYCODE_KEY_MOVE_HOME = 2081,
    ARKUI_KEYCODE_KEY_MOVE_END = 2082,
    ARKUI_KEYCODE_KEY_INSERT = 2083,
    ARKUI_KEYCODE_KEY_FORWARD = 2084,
    ARKUI_KEYCODE_KEY_MEDIA_PLAY = 2085,
    ARKUI_KEYCODE_KEY_MEDIA_PAUSE = 2086,
    ARKUI_KEYCODE_KEY_MEDIA_CLOSE = 2087,
    ARKUI_KEYCODE_KEY_MEDIA_EJECT = 2088,
    ARKUI_KEYCODE_KEY_MEDIA_RECORD = 2089,
    ARKUI_KEYCODE_KEY_F1 = 2090,
    ARKUI_KEYCODE_KEY_F2 = 2091,
    ARKUI_KEYCODE_KEY_F3 = 2092,
    ARKUI_KEYCODE_KEY_F4 = 2093,
    ARKUI_KEYCODE_KEY_F5 = 2094,
    ARKUI_KEYCODE_KEY_F6 = 2095,
    ARKUI_KEYCODE_KEY_F7 = 2096,
    ARKUI_KEYCODE_KEY_F8 = 2097,
    ARKUI_KEYCODE_KEY_F9 = 2098,
    ARKUI_KEYCODE_KEY_F10 = 2099,
    ARKUI_KEYCODE_KEY_F11 = 2100,
    ARKUI_KEYCODE_KEY_F12 = 2101,
    ARKUI_KEYCODE_KEY_NUM_LOCK = 2102,
    ARKUI_KEYCODE_KEY_NUMPAD_0 = 2103,
    ARKUI_KEYCODE_KEY_NUMPAD_1 = 2104,
    ARKUI_KEYCODE_KEY_NUMPAD_2 = 2105,
    ARKUI_KEYCODE_KEY_NUMPAD_3 = 2106,
    ARKUI_KEYCODE_KEY_NUMPAD_4 = 2107,
    ARKUI_KEYCODE_KEY_NUMPAD_5 = 2108,
    ARKUI_KEYCODE_KEY_NUMPAD_6 = 2109,
    ARKUI_KEYCODE_KEY_NUMPAD_7 = 2110,
    ARKUI_KEYCODE_KEY_NUMPAD_8 = 2111,
    ARKUI_KEYCODE_KEY_NUMPAD_9 = 2112,
    ARKUI_KEYCODE_KEY_NUMPAD_DIVIDE = 2113,
    ARKUI_KEYCODE_KEY_NUMPAD_MULTIPLY = 2114,
    ARKUI_KEYCODE_KEY_NUMPAD_SUBTRACT = 2115,
    ARKUI_KEYCODE_KEY_NUMPAD_ADD = 2116,
    ARKUI_KEYCODE_KEY_NUMPAD_DOT = 2117,
    ARKUI_KEYCODE_KEY_NUMPAD_COMMA = 2118,
    ARKUI_KEYCODE_KEY_NUMPAD_ENTER = 2119,
    ARKUI_KEYCODE_KEY_NUMPAD_EQUALS = 2120,
    ARKUI_KEYCODE_KEY_NUMPAD_LEFT_PAREN = 2121,
    ARKUI_KEYCODE_KEY_NUMPAD_RIGHT_PAREN = 2122,

    ARKUI_KEYCODE_KEY_VIRTUAL_MULTITASK = 2210,
    ARKUI_KEYCODE_KEY_BUTTON_A = 2301,
    ARKUI_KEYCODE_KEY_BUTTON_B = 2302,
    ARKUI_KEYCODE_KEY_BUTTON_C = 2303,
    ARKUI_KEYCODE_KEY_BUTTON_X = 2304,
    ARKUI_KEYCODE_KEY_BUTTON_Y = 2305,
    ARKUI_KEYCODE_KEY_BUTTON_Z = 2306,
    ARKUI_KEYCODE_KEY_BUTTON_L1 = 2307,
    ARKUI_KEYCODE_KEY_BUTTON_R1 = 2308,
    ARKUI_KEYCODE_KEY_BUTTON_L2 = 2309,
    ARKUI_KEYCODE_KEY_BUTTON_R2 = 2310,
    ARKUI_KEYCODE_KEY_BUTTON_SELECT = 2311,
    ARKUI_KEYCODE_KEY_BUTTON_START = 2312,
    ARKUI_KEYCODE_KEY_BUTTON_MODE = 2313,
    ARKUI_KEYCODE_KEY_BUTTON_THUMBL = 2314,
    ARKUI_KEYCODE_KEY_BUTTON_THUMBR = 2315,
    ARKUI_KEYCODE_KEY_BUTTON_TRIGGER = 2401,
    ARKUI_KEYCODE_KEY_BUTTON_THUMB = 2402,
    ARKUI_KEYCODE_KEY_BUTTON_THUMB2 = 2403,
    ARKUI_KEYCODE_KEY_BUTTON_TOP = 2404,
    ARKUI_KEYCODE_KEY_BUTTON_TOP2 = 2405,
    ARKUI_KEYCODE_KEY_BUTTON_PINKIE = 2406,
    ARKUI_KEYCODE_KEY_BUTTON_BASE1 = 2407,
    ARKUI_KEYCODE_KEY_BUTTON_BASE2 = 2408,
    ARKUI_KEYCODE_KEY_BUTTON_BASE3 = 2409,
    ARKUI_KEYCODE_KEY_BUTTON_BASE4 = 2410,
    ARKUI_KEYCODE_KEY_BUTTON_BASE5 = 2411,
    ARKUI_KEYCODE_KEY_BUTTON_BASE6 = 2412,
    ARKUI_KEYCODE_KEY_BUTTON_BASE7 = 2413,
    ARKUI_KEYCODE_KEY_BUTTON_BASE8 = 2414,
    ARKUI_KEYCODE_KEY_BUTTON_BASE9 = 2415,
    ARKUI_KEYCODE_KEY_BUTTON_DEAD = 2416,
    ARKUI_KEYCODE_KEY_SLEEP = 2600,
    ARKUI_KEYCODE_KEY_ZENKAKU_HANKAKU = 2601,
    ARKUI_KEYCODE_KEY_102ND = 2602,
    ARKUI_KEYCODE_KEY_RO = 2603,
    ARKUI_KEYCODE_KEY_KATAKANA = 2604,
    ARKUI_KEYCODE_KEY_HIRAGANA = 2605,
    ARKUI_KEYCODE_KEY_HENKAN = 2606,
    ARKUI_KEYCODE_KEY_KATAKANA_HIRAGANA = 2607,
    ARKUI_KEYCODE_KEY_MUHENKAN = 2608,
    ARKUI_KEYCODE_KEY_LINEFEED = 2609,
    ARKUI_KEYCODE_KEY_MACRO = 2610,
    ARKUI_KEYCODE_KEY_NUMPAD_PLUSMINUS = 2611,
    ARKUI_KEYCODE_KEY_SCALE = 2612,
    ARKUI_KEYCODE_KEY_HANGUEL = 2613,
    ARKUI_KEYCODE_KEY_HANJA = 2614,
    ARKUI_KEYCODE_KEY_YEN = 2615,
    ARKUI_KEYCODE_KEY_STOP = 2616,
    ARKUI_KEYCODE_KEY_AGAIN = 2617,
    ARKUI_KEYCODE_KEY_PROPS = 2618,
    ARKUI_KEYCODE_KEY_UNDO = 2619,
    ARKUI_KEYCODE_KEY_COPY = 2620,
    ARKUI_KEYCODE_KEY_OPEN = 2621,
    ARKUI_KEYCODE_KEY_PASTE = 2622,
    ARKUI_KEYCODE_KEY_FIND = 2623,
    ARKUI_KEYCODE_KEY_CUT = 2624,
    ARKUI_KEYCODE_KEY_HELP = 2625,
    ARKUI_KEYCODE_KEY_CALC = 2626,
    ARKUI_KEYCODE_KEY_FILE = 2627,
    ARKUI_KEYCODE_KEY_BOOKMARKS = 2628,
    ARKUI_KEYCODE_KEY_NEXT = 2629,
    ARKUI_KEYCODE_KEY_PLAYPAUSE = 2630,
    ARKUI_KEYCODE_KEY_PREVIOUS = 2631,
    ARKUI_KEYCODE_KEY_STOPCD = 2632,
    ARKUI_KEYCODE_KEY_CONFIG = 2634,
    ARKUI_KEYCODE_KEY_REFRESH = 2635,
    ARKUI_KEYCODE_KEY_EXIT = 2636,
    ARKUI_KEYCODE_KEY_EDIT = 2637,
    ARKUI_KEYCODE_KEY_SCROLLUP = 2638,
    ARKUI_KEYCODE_KEY_SCROLLDOWN = 2639,
    ARKUI_KEYCODE_KEY_NEW = 2640,
    ARKUI_KEYCODE_KEY_REDO = 2641,
    ARKUI_KEYCODE_KEY_CLOSE = 2642,
    ARKUI_KEYCODE_KEY_PLAY = 2643,
    ARKUI_KEYCODE_KEY_BASSBOOST = 2644,
    ARKUI_KEYCODE_KEY_PRINT = 2645,
    ARKUI_KEYCODE_KEY_CHAT = 2646,
    ARKUI_KEYCODE_KEY_FINANCE = 2647,
    ARKUI_KEYCODE_KEY_CANCEL = 2648,
    ARKUI_KEYCODE_KEY_KBDILLUM_TOGGLE = 2649,
    ARKUI_KEYCODE_KEY_KBDILLUM_DOWN = 2650,
    ARKUI_KEYCODE_KEY_KBDILLUM_UP = 2651,
    ARKUI_KEYCODE_KEY_SEND = 2652,
    ARKUI_KEYCODE_KEY_REPLY = 2653,
    ARKUI_KEYCODE_KEY_FORWARDMAIL = 2654,
    ARKUI_KEYCODE_KEY_SAVE = 2655,
    ARKUI_KEYCODE_KEY_DOCUMENTS = 2656,
    ARKUI_KEYCODE_KEY_VIDEO_NEXT = 2657,
    ARKUI_KEYCODE_KEY_VIDEO_PREV = 2658,
    ARKUI_KEYCODE_KEY_BRIGHTNESS_CYCLE = 2659,
    ARKUI_KEYCODE_KEY_BRIGHTNESS_ZERO = 2660,
    ARKUI_KEYCODE_KEY_DISPLAY_OFF = 2661,
    ARKUI_KEYCODE_KEY_BTN_MISC = 2662,
    ARKUI_KEYCODE_KEY_GOTO = 2663,
    ARKUI_KEYCODE_KEY_INFO = 2664,
    ARKUI_KEYCODE_KEY_PROGRAM = 2665,
    ARKUI_KEYCODE_KEY_PVR = 2666,
    ARKUI_KEYCODE_KEY_SUBTITLE = 2667,
    ARKUI_KEYCODE_KEY_FULL_SCREEN = 2668,
    ARKUI_KEYCODE_KEY_KEYBOARD = 2669,
    ARKUI_KEYCODE_KEY_ASPECT_RATIO = 2670,
    ARKUI_KEYCODE_KEY_PC = 2671,
    ARKUI_KEYCODE_KEY_TV = 2672,
    ARKUI_KEYCODE_KEY_TV2 = 2673,
    ARKUI_KEYCODE_KEY_VCR = 2674,
    ARKUI_KEYCODE_KEY_VCR2 = 2675,
    ARKUI_KEYCODE_KEY_SAT = 2676,
    ARKUI_KEYCODE_KEY_CD = 2677,
    ARKUI_KEYCODE_KEY_TAPE = 2678,
    ARKUI_KEYCODE_KEY_TUNER = 2679,
    ARKUI_KEYCODE_KEY_PLAYER = 2680,
    ARKUI_KEYCODE_KEY_DVD = 2681,
    ARKUI_KEYCODE_KEY_AUDIO = 2682,
    ARKUI_KEYCODE_KEY_VIDEO = 2683,
    ARKUI_KEYCODE_KEY_MEMO = 2684,
    ARKUI_KEYCODE_KEY_CALENDAR = 2685,
    ARKUI_KEYCODE_KEY_RED = 2686,
    ARKUI_KEYCODE_KEY_GREEN = 2687,
    ARKUI_KEYCODE_KEY_YELLOW = 2688,
    ARKUI_KEYCODE_KEY_BLUE = 2689,
    ARKUI_KEYCODE_KEY_CHANNELUP = 2690,
    ARKUI_KEYCODE_KEY_CHANNELDOWN = 2691,
    ARKUI_KEYCODE_KEY_LAST = 2692,
    ARKUI_KEYCODE_KEY_RESTART = 2693,
    ARKUI_KEYCODE_KEY_SLOW = 2694,
    ARKUI_KEYCODE_KEY_SHUFFLE = 2695,
    ARKUI_KEYCODE_KEY_VIDEOPHONE = 2696,
    ARKUI_KEYCODE_KEY_GAMES = 2697,
    ARKUI_KEYCODE_KEY_ZOOMIN = 2698,
    ARKUI_KEYCODE_KEY_ZOOMOUT = 2699,
    ARKUI_KEYCODE_KEY_ZOOMRESET = 2700,
    ARKUI_KEYCODE_KEY_WORDPROCESSOR = 2701,
    ARKUI_KEYCODE_KEY_EDITOR = 2702,
    ARKUI_KEYCODE_KEY_SPREADSHEET = 2703,
    ARKUI_KEYCODE_KEY_GRAPHICSEDITOR = 2704,
    ARKUI_KEYCODE_KEY_PRESENTATION = 2705,
    ARKUI_KEYCODE_KEY_DATABASE = 2706,
    ARKUI_KEYCODE_KEY_NEWS = 2707,
    ARKUI_KEYCODE_KEY_VOICEMAIL = 2708,
    ARKUI_KEYCODE_KEY_ADDRESSBOOK = 2709,
    ARKUI_KEYCODE_KEY_MESSENGER = 2710,
    ARKUI_KEYCODE_KEY_BRIGHTNESS_TOGGLE = 2711,
    ARKUI_KEYCODE_KEY_SPELLCHECK = 2712,
    ARKUI_KEYCODE_KEY_COFFEE = 2713,
    ARKUI_KEYCODE_KEY_MEDIA_REPEAT = 2714,
    ARKUI_KEYCODE_KEY_IMAGES = 2715,
    ARKUI_KEYCODE_KEY_BUTTONCONFIG = 2716,
    ARKUI_KEYCODE_KEY_TASKMANAGER = 2717,
    ARKUI_KEYCODE_KEY_JOURNAL = 2718,
    ARKUI_KEYCODE_KEY_CONTROLPANEL = 2719,
    ARKUI_KEYCODE_KEY_APPSELECT = 2720,
    ARKUI_KEYCODE_KEY_SCREENSAVER = 2721,
    ARKUI_KEYCODE_KEY_ASSISTANT = 2722,
    ARKUI_KEYCODE_KEY_KBD_LAYOUT_NEXT = 2723,
    ARKUI_KEYCODE_KEY_BRIGHTNESS_MIN = 2724,
    ARKUI_KEYCODE_KEY_BRIGHTNESS_MAX = 2725,
    ARKUI_KEYCODE_KEY_KBDINPUTASSIST_PREV = 2726,
    ARKUI_KEYCODE_KEY_KBDINPUTASSIST_NEXT = 2727,
    ARKUI_KEYCODE_KEY_KBDINPUTASSIST_PREVGROUP = 2728,
    ARKUI_KEYCODE_KEY_KBDINPUTASSIST_NEXTGROUP = 2729,
    ARKUI_KEYCODE_KEY_KBDINPUTASSIST_ACCEPT = 2730,
    ARKUI_KEYCODE_KEY_KBDINPUTASSIST_CANCEL = 2731,
    ARKUI_KEYCODE_KEY_FRONT = 2800,
    ARKUI_KEYCODE_KEY_SETUP = 2801,
    ARKUI_KEYCODE_KEY_WAKEUP = 2802,
    ARKUI_KEYCODE_KEY_SENDFILE = 2803,
    ARKUI_KEYCODE_KEY_DELETEFILE = 2804,
    ARKUI_KEYCODE_KEY_XFER = 2805,
    ARKUI_KEYCODE_KEY_PROG1 = 2806,
    ARKUI_KEYCODE_KEY_PROG2 = 2807,
    ARKUI_KEYCODE_KEY_MSDOS = 2808,
    ARKUI_KEYCODE_KEY_SCREENLOCK = 2809,
    ARKUI_KEYCODE_KEY_DIRECTION_ROTATE_DISPLAY = 2810,
    ARKUI_KEYCODE_KEY_CYCLEWINDOWS = 2811,
    ARKUI_KEYCODE_KEY_COMPUTER = 2812,
    ARKUI_KEYCODE_KEY_EJECTCLOSECD = 2813,
    ARKUI_KEYCODE_KEY_ISO = 2814,
    ARKUI_KEYCODE_KEY_MOVE = 2815,
    ARKUI_KEYCODE_KEY_F13 = 2816,
    ARKUI_KEYCODE_KEY_F14 = 2817,
    ARKUI_KEYCODE_KEY_F15 = 2818,
    ARKUI_KEYCODE_KEY_F16 = 2819,
    ARKUI_KEYCODE_KEY_F17 = 2820,
    ARKUI_KEYCODE_KEY_F18 = 2821,
    ARKUI_KEYCODE_KEY_F19 = 2822,
    ARKUI_KEYCODE_KEY_F20 = 2823,
    ARKUI_KEYCODE_KEY_F21 = 2824,
    ARKUI_KEYCODE_KEY_F22 = 2825,
    ARKUI_KEYCODE_KEY_F23 = 2826,
    ARKUI_KEYCODE_KEY_F24 = 2827,
    ARKUI_KEYCODE_KEY_PROG3 = 2828,
    ARKUI_KEYCODE_KEY_PROG4 = 2829,
    ARKUI_KEYCODE_KEY_DASHBOARD = 2830,
    ARKUI_KEYCODE_KEY_SUSPEND = 2831,
    ARKUI_KEYCODE_KEY_HP = 2832,
    ARKUI_KEYCODE_KEY_SOUND = 2833,
    ARKUI_KEYCODE_KEY_QUESTION = 2834,
    ARKUI_KEYCODE_KEY_CONNECT = 2836,
    ARKUI_KEYCODE_KEY_SPORT = 2837,
    ARKUI_KEYCODE_KEY_SHOP = 2838,
    ARKUI_KEYCODE_KEY_ALTERASE = 2839,
    ARKUI_KEYCODE_KEY_SWITCHVIDEOMODE = 2841,
    ARKUI_KEYCODE_KEY_BATTERY = 2842,
    ARKUI_KEYCODE_KEY_BLUETOOTH = 2843,
    ARKUI_KEYCODE_KEY_WLAN = 2844,
    ARKUI_KEYCODE_KEY_UWB = 2845,
    ARKUI_KEYCODE_KEY_WWAN_WIMAX = 2846,
    ARKUI_KEYCODE_KEY_RFKILL = 2847,
    ARKUI_KEYCODE_KEY_CHANNEL = 3001,
    ARKUI_KEYCODE_KEY_BTN_0 = 3100,
    ARKUI_KEYCODE_KEY_BTN_1 = 3101,
    ARKUI_KEYCODE_KEY_BTN_2 = 3102,
    ARKUI_KEYCODE_KEY_BTN_3 = 3103,
    ARKUI_KEYCODE_KEY_BTN_4 = 3104,
    ARKUI_KEYCODE_KEY_BTN_5 = 3105,
    ARKUI_KEYCODE_KEY_BTN_6 = 3106,
    ARKUI_KEYCODE_KEY_BTN_7 = 3107,
    ARKUI_KEYCODE_KEY_BTN_8 = 3108,
    ARKUI_KEYCODE_KEY_BTN_9 = 3109,
    ARKUI_KEYCODE_KEY_BRL_DOT1 = 3201,
    ARKUI_KEYCODE_KEY_BRL_DOT2 = 3202,
    ARKUI_KEYCODE_KEY_BRL_DOT3 = 3203,
    ARKUI_KEYCODE_KEY_BRL_DOT4 = 3204,
    ARKUI_KEYCODE_KEY_BRL_DOT5 = 3205,
    ARKUI_KEYCODE_KEY_BRL_DOT6 = 3206,
    ARKUI_KEYCODE_KEY_BRL_DOT7 = 3207,
    ARKUI_KEYCODE_KEY_BRL_DOT8 = 3208,
    ARKUI_KEYCODE_KEY_BRL_DOT9 = 3209,
    ARKUI_KEYCODE_KEY_BRL_DOT10 = 3210,

    ARKUI_KEYCODE_KEY_LEFT_KNOB_ROLL_UP = 10001,
    ARKUI_KEYCODE_KEY_LEFT_KNOB_ROLL_DOWN = 10002,
    ARKUI_KEYCODE_KEY_LEFT_KNOB = 10003,
    ARKUI_KEYCODE_KEY_RIGHT_KNOB_ROLL_UP = 10004,
    ARKUI_KEYCODE_KEY_RIGHT_KNOB_ROLL_DOWN = 10005,
    ARKUI_KEYCODE_KEY_RIGHT_KNOB = 10006,
    ARKUI_KEYCODE_KEY_VOICE_SOURCE_SWITCH = 10007,
    ARKUI_KEYCODE_KEY_LAUNCHER_MENU = 10008,

    ARKUI_KEYCODE_TV_CONTROL_BACK = ARKUI_KEYCODE_KEY_BACK,
    ARKUI_KEYCODE_TV_CONTROL_UP = ARKUI_KEYCODE_KEY_DPAD_UP,
    ARKUI_KEYCODE_TV_CONTROL_DOWN = ARKUI_KEYCODE_KEY_DPAD_DOWN,
    ARKUI_KEYCODE_TV_CONTROL_LEFT = ARKUI_KEYCODE_KEY_DPAD_LEFT,
    ARKUI_KEYCODE_TV_CONTROL_RIGHT = ARKUI_KEYCODE_KEY_DPAD_RIGHT,
    ARKUI_KEYCODE_TV_CONTROL_CENTER = ARKUI_KEYCODE_KEY_DPAD_CENTER,
    ARKUI_KEYCODE_TV_CONTROL_ENTER = ARKUI_KEYCODE_KEY_ENTER,
    ARKUI_KEYCODE_TV_CONTROL_MEDIA_PLAY = ARKUI_KEYCODE_KEY_MEDIA_PLAY,
} ArkUI_KeyCode_E;

/**
 * @brief 按键的类型。
 *
 * @since 14
 */
typedef enum {
    ARKUI_KEY_EVENT_UNKNOWN = -1,
    ARKUI_KEY_EVENT_DOWN = 0,
    ARKUI_KEY_EVENT_UP = 1,
    ARKUI_KEY_EVENT_LONG_PRESS = 2,
    ARKUI_KEY_EVENT_CLICK = 3,
} ArkUI_KeyEventType_E;

/**
 * @brief 触发当前按键的输入设备类型。
 *
 * @since 14
 */
typedef enum {
    ARKUI_KEY_SOURCE_UNKNOWN = 0,
    ARKUI_KEY_SOURCE_TYPE_MOUSE = 1,
    ARKUI_KEY_SOURCE_TYPE_TOUCH = 2,
    ARKUI_KEY_SOURCE_TYPE_TOUCH_PAD = 3,
    ARKUI_KEY_SOURCE_TYPE_KEYBOARD = 4,
} ArkUI_KeySourceType_E;

/**
 * @brief 按键对应的意图。
 *
 * @since 14
 */
typedef enum {
    ARKUI_KEY_INTENSION_UNKNOWN = -1,
    ARKUI_KEY_INTENSION_UP = 1,
    ARKUI_KEY_INTENSION_DOWN = 2,
    ARKUI_KEY_INTENSION_LEFT = 3,
    ARKUI_KEY_INTENSION_RIGHT = 4,
    ARKUI_KEY_INTENSION_SELECT = 5,
    ARKUI_KEY_INTENSION_ESCAPE = 6,
    ARKUI_KEY_INTENSION_BACK = 7,
    ARKUI_KEY_INTENSION_FORWARD = 8,
    ARKUI_KEY_INTENSION_MENU = 9,
    ARKUI_KEY_INTENSION_HOME = 10,
    ARKUI_KEY_INTENSION_PAGE_UP = 11,
    ARKUI_KEY_INTENSION_PAGE_DOWN = 12,
    ARKUI_KEY_INTENSION_ZOOM_OUT = 13,
    ARKUI_KEY_INTENSION_ZOOM_IN = 14,

    ARKUI_KEY_INTENTION_MEDIA_PLAY_PAUSE = 100,
    ARKUI_KEY_INTENTION_MEDIA_FAST_FORWARD = 101,
    ARKUI_KEY_INTENTION_MEDIA_FAST_REWIND = 102,
    ARKUI_KEY_INTENTION_MEDIA_FAST_PLAYBACK = 103,
    ARKUI_KEY_INTENTION_MEDIA_NEXT = 104,
    ARKUI_KEY_INTENTION_MEDIA_PREVIOUS = 105,
    ARKUI_KEY_INTENTION_MEDIA_MUTE = 106,
    ARKUI_KEY_INTENTION_VOLUTE_UP = 107,
    ARKUI_KEY_INTENTION_VOLUTE_DOWN = 108,

    ARKUI_KEY_INTENTION_CALL = 200,
    ARKUI_KEY_INTENTION_ENDCALL = 201,
    ARKUI_KEY_INTENTION_REJECTCALL = 202,

    ARKUI_KEY_INTENTION_CAMERA = 300,
} ArkUI_KeyIntension_E;

/**
 * @brief 获取按键的类型。
 *
 * @param event ArkUI_UIInputEvent事件指针。
 * @return ArkUI_KeyEventType_E 按键的类型。
 * @since 14
 */
ArkUI_KeyEventType_E OH_ArkUI_KeyEvent_GetType(const ArkUI_UIInputEvent* event);

/**
 * @brief 获取按键的键码。
 *
 * @param event ArkUI_UIInputEvent事件指针。
 * @return 按键的键码。
 * @since 14
 */
ArkUI_KeyCode_E OH_ArkUI_KeyEvent_GetKeyCode(const ArkUI_UIInputEvent* event);

/**
 * @brief 获取按键的键值。
 *
 * @param event ArkUI_UIInputEvent事件指针。
 * @return 按键的键值。
 * @since 14
 */
const char* OH_ArkUI_KeyEvent_GetKeyText(const ArkUI_UIInputEvent* event);

/**
 * @brief 获取当前按键的输入设备类型。
 *
 * @param event ArkUI_UIInputEvent事件指针。
 * @return ArkUI_KeySourceType_E 当前按键的输入设备类型。
 * @since 14
 */
ArkUI_KeySourceType_E OH_ArkUI_KeyEvent_GetKeySource(const ArkUI_UIInputEvent* event);

/**
 * @brief 获取当前按键的输入设备ID。
 *
 * @param event ArkUI_UIInputEvent事件指针。
 * @return 当前按键的输入设备ID。
 * @since 14
 */
int32_t OH_ArkUI_KeyEvent_GetDeviceId(const ArkUI_UIInputEvent* event);

/**
 * @brief 获取按键发生时元键（即Windows键盘的WIN键、Mac键盘的Command键）的状态。
 *
 * @param event ArkUI_UIInputEvent事件指针。
 * @return 按键发生时元键（即Windows键盘的WIN键、Mac键盘的Command键）的状态，1表示按压态，0表示未按压态。
 * @since 14
 */
int32_t OH_ArkUI_KeyEvent_GetMetaKey(const ArkUI_UIInputEvent* event);

/**
 * @brief 获取事件时间戳。触发事件时距离系统启动的时间间隔，单位：ns。
 *
 * @param event ArkUI_UIInputEvent事件指针。
 * @return 事件时间戳，单位：ns。
 * @since 14
 */
uint64_t OH_ArkUI_KeyEvent_GetTimestamp(const ArkUI_UIInputEvent* event);

/**
 * @brief 阻塞事件冒泡传递。
 *
 * @param event ArkUI_UIInputEvent事件指针。
 * @param stopPropagation 表示是否阻止事件冒泡。
 * @since 14
 */
void OH_ArkUI_KeyEvent_StopPropagation(const ArkUI_UIInputEvent* event, bool stopPropagation);

/**
 * @brief 获取功能键按压状态。报错信息请参考以下错误码。支持功能键 'Ctrl'|'Alt'|'Shift'|'Fn'，设备外接带Fn键的键盘不支持Fn键查询。
 *
 * @param event ArkUI_UIInputEvent事件指针。
 * @param uint32_t modifierKeys 应用在输入时需通过 {@link ArkUI_ModifierKeyName} 创建。
 * @return bool 是否按压。
 * @since 14
 */
bool OH_ArkUI_KeyEvent_GetModifierKeyState(const ArkUI_UIInputEvent* event, uint32_t modifierKeys);

/**
 * @brief 获取按键对应的意图。
 *
 * @param event ArkUI_UIInputEvent事件指针。
 * @return ArkUI_KeyIntension_E 按键对应的意图。
 * @since 14
 */
ArkUI_KeyIntension_E OH_ArkUI_KeyEvent_GetKeyIntensionCode(const ArkUI_UIInputEvent* event);

/**
 * @brief 获取按键的unicode码值。支持范围为非空格的基本拉丁字符：0x0021-0x007E，不支持字符为0。组合键场景下，返回当前keyEvent对应按键的unicode码值。
 *
 * @param event ArkUI_UIInputEvent事件指针。
 * @return unicode码值。
 * @since 14
 */
uint32_t OH_ArkUI_KeyEvent_GetUnicode(const ArkUI_UIInputEvent* event);

/**
 * @brief 在按键事件回调中，设置事件是否被该回调消费
 *
 * @param event ArkUI_UIInputEvent事件指针。
 * @param ret 是否被消费。
 * @since 14
 */
void OH_ArkUI_KeyEvent_SetRet(const ArkUI_UIInputEvent* event, bool ret);
#ifdef __cplusplus
};
#endif

#endif // ARKUI_NATIVE_KEY_EVENT_H